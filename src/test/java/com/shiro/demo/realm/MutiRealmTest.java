package com.shiro.demo.realm;

import static org.junit.Assert.*;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.util.Factory;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.shiro.demo.test.util.AbstractShiroTest;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class MutiRealmTest extends AbstractShiroTest {
	
	@BeforeClass
	public static void testInitialization() {
		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiroInitForMultiRealmTest.ini");
		setSecurityManager(factory.getInstance());
	}

	@Test
	public void testLoginForAdmin() {
		login(admin, passwordForAdmin);
		assertTrue(currentUser.isAuthenticated());
		assertTrue(currentUser.isPermittedAll("linux:create", "linux:update", "linux:read", "linux:delete"));
		assertFalse(currentUser.isPermitted("eat"));
		logout();
		assertFalse(currentUser.isAuthenticated());
	}
	
	@Test(expected = AuthenticationException.class)
	public void testAuthenticationFailedForBadGuy() {
		System.out.println("Here comes the Miffy!!!");
		login(badGuy, passwordForBadGuy);
		System.out.println("Miffy arrived!!!");
	}
	
	@After
	public void tearDownSubject() {
		clearSubject();
	}

}
