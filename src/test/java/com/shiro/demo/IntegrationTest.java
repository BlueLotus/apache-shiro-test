package com.shiro.demo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.shiro.demo.cipher.gen.EncryptorTest;
import com.shiro.demo.dao.SessionDaoTest;
import com.shiro.demo.realm.MutiRealmTest;
import com.shiro.demo.test.AuthenticationTest;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ AuthenticationTest.class,
				MutiRealmTest.class, 
				EncryptorTest.class, 
				SessionDaoTest.class })
public class IntegrationTest {

}
