package com.shiro.demo.cipher.gen;

import static org.junit.Assert.*;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class EncryptorTest {

	private Encryptor encryptor;
	private String originalPassword;
	private String encryptedPassword;
	private String encryptionAlgorithm;
	
	@Before
	public void init() {
		encryptor = new Encryptor();
		originalPassword = "qwert1234";
		encryptionAlgorithm = "none";
	}
	
	@Test
	public void encryptionTestWithoutAlg() {
		encryptedPassword = encryptor.obtainPasswordAfterEncryption(originalPassword, encryptionAlgorithm);
		assertTrue(originalPassword.equals(encryptedPassword));
	}
	
	@Test
	@Ignore
	public void encryptionTestWithSHA256() {
		encryptionAlgorithm = "SHA256";
		encryptedPassword = encryptor.obtainPasswordAfterEncryption(originalPassword, encryptionAlgorithm);
		assertFalse(originalPassword.equals(encryptedPassword));
	}
	
	@Test
	@Ignore
	public void encryptionWithSalt() {
		System.out.println(new Sha256Hash("qwert1234", "carl", 20).toHex());
	}

}
