package com.shiro.demo.dao;

import static org.junit.Assert.*;

import java.io.Serializable;

import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.util.Factory;
import org.junit.BeforeClass;
import org.junit.Test;

import com.shiro.demo.test.util.AbstractShiroTest;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class SessionDaoTest extends AbstractShiroTest {
	
	private String sessionKey = "Level";
	private String sessionValue = "VIP";
	private Serializable currentSessionId;
	
	@BeforeClass
	public static void initBeforeClass() {
		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiroInitForSessionDaoTest.ini");
		setSecurityManager(factory.getInstance());
	}

	@Test
	public void testForSessionAttributeOperation() {
		login(admin, passwordForAdmin);
		currentSessionId = currentUser.getSession().getId();
		currentUser.getSession().setAttribute(sessionKey, sessionValue);
		assertTrue(currentUser.getSession().getAttribute(sessionKey).toString().equals(sessionValue));
		logout();
		assertFalse(currentUser.getSession().getId().equals(currentSessionId));
		assertNull(currentUser.getSession().getAttribute(sessionKey));
	}

}
