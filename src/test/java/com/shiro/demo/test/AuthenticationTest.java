package com.shiro.demo.test;

import static org.junit.Assert.*;

import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.util.Factory;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shiro.demo.test.util.AbstractShiroTest;
import com.shiro.demo.test.util.Order;
import com.shiro.demo.test.util.OrderedRunner;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@RunWith(OrderedRunner.class)
public class AuthenticationTest extends AbstractShiroTest{
	
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationTest.class);
	
	@BeforeClass
	public static void testInitialization() {
		logger.info("Loading ini file for authentication test...");
		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiroInitForAuthenticationTest.ini");
		setSecurityManager(factory.getInstance());
		logger.info("Loading successfully...");
	}
	
	@Test
	@Order(order = 1)
	public void testForAdminLogin() {
		login(admin, passwordForAdmin);
		assertTrue(currentUser.isAuthenticated());
		assertTrue(currentUser.hasRole("admin"));
		logout();
		assertFalse(currentUser.isAuthenticated());
	}
	
	@Test
	@Order(order = 2)
	public void testForGeneralUserLogin() {
		login(generalUser, passwordForGeneralUser);
		assertTrue(currentUser.isAuthenticated());
		assertTrue(currentUser.hasRole("generaluser"));
		logout();
		assertFalse(currentUser.isAuthenticated());
	}
	
	@Test
	@Order(order = 3)
	public void testForAdminPermissionCheck() {
		login(admin, passwordForAdmin);
		assertTrue(currentUser.isAuthenticated());
		assertTrue(currentUser.isPermittedAll("linux:create", "linux:update", "linux:read", "linux:delete"));
		assertFalse(currentUser.isPermitted("eat"));
		logout();
		assertFalse(currentUser.isAuthenticated());
	}
	
	@Test
	@Order(order = 4)
	public void testForGeneralUSerPermissionCheck() {
		login(generalUser, passwordForGeneralUser);
		assertTrue(currentUser.isAuthenticated());
		assertFalse(currentUser.isPermittedAll("linux:create", "linux:update", "linux:read", "linux:delete"));
		assertTrue(currentUser.isPermitted("linux:read"));
		logout();
		assertFalse(currentUser.isAuthenticated());
	}
	
	@Test(expected = IncorrectCredentialsException.class)
	@Order(order = 5)
	public void testForInvalidLogin() {
		login(admin, passwordForGeneralUser);
	}
	
	@After
	public void tearDownSubject() {
		clearSubject();
	}

}
