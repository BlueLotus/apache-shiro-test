package com.shiro.demo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.ExpiredSessionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class HelloShiro {
	
	private static final Logger logger = LoggerFactory.getLogger(HelloShiro.class);
	
	public static void main(String[] args) {
		
		String admin = "carl";
		String operator = "ruru";
		String passwordForAdmin = "qwert1234";
		String passwordForOperator = "penguin";
		
		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiroinit.ini");
		
		SecurityManager securityManager = factory.getInstance();
		
		SecurityUtils.setSecurityManager(securityManager);
		
		UsernamePasswordToken token = new UsernamePasswordToken(admin, passwordForAdmin);
		token.setRememberMe(true);
		logger.debug("Before create subject...");
		Subject currentUser = SecurityUtils.getSubject();
		logger.debug("Session before login: {}", currentUser.getSession().getId());
		logger.debug("{}", currentUser.getSession().getAttribute("Name"));
		currentUser.login(token);
		logger.debug("Session after login: {}", currentUser.getSession().getId());
		
		currentUser.getSession().setAttribute("Name", "Carl");
		logger.debug("{}", currentUser.getSession().getAttribute("Name"));
		
		logger.debug("Current principal: {}", currentUser.getPrincipal());
		logger.debug("is remeber: {}", currentUser.isRemembered());
		logger.debug("is authenticated: {}", currentUser.isAuthenticated());
		
		boolean permissionFlag = currentUser.isPermittedAll("linux:create:redhat", "linux:update:redhat");
		logger.debug("Permission on linux: {}", permissionFlag);
		logger.debug("Permission for anything on windows: {}", currentUser.isPermittedAll("windows:create:win7", "windows:delete:winxp", "windows:read", "windows:update"));
		logger.debug("All permissions for admin on macosx leopard: {}", currentUser.isPermittedAll("macosx:create:leopard", "macosx:read:leopard"));
		logger.debug("All permissions for admin on macosx 2: {}", currentUser.isPermittedAll("macosx:create:l2"));
		try {
			Thread.sleep(2000);
		} catch (ExpiredSessionException | InterruptedException e) {
			e.printStackTrace();
		}
		logger.debug("Has operator: {}", currentUser.hasRole("operator"));
		currentUser.logout();
		logger.debug("Session after logout: {}", currentUser.getSession().getId());
		logger.debug("{}",currentUser.getSession().getAttribute("Name"));
		
		token = new UsernamePasswordToken(operator, passwordForOperator);
		currentUser = SecurityUtils.getSubject();
		currentUser.login(token);
		logger.debug("Create permission for operator on linux: {}", currentUser.isPermitted("linux:create"));
		logger.debug("Create permission for operator on windows: {}", currentUser.isPermitted("windows:create"));
		logger.debug("Read permission for operator on anywhere: {}", currentUser.isPermittedAll("linux:read", "windows:read"));
		logger.debug("Has Admiin: {}", currentUser.hasRole("admin"));
		currentUser.logout();
		
	}
	
}
