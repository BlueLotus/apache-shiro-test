package com.shiro.demo.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class CustomSessionDAO extends AbstractSessionDAO {
	
	private Map<Serializable, Session> sessionBuffer = new HashMap<Serializable, Session>();

	@Override
	public void update(Session session) throws UnknownSessionException {
		System.out.println("Updating session...");
		sessionBuffer.put(session.getId(), session);
	}

	@Override
	public void delete(Session session) {
		System.out.println("Deleteing session...");
		sessionBuffer.remove(session.getId());
	}

	@Override
	public Collection<Session> getActiveSessions() {
		System.out.println("Get active sessions...");
		return sessionBuffer.values();
	}

	@Override
	protected Serializable doCreate(Session session) {
		System.out.println("Creating session...");
		Serializable sessionId = generateSessionId(session);
		assignSessionId(session, sessionId);
		sessionBuffer.put(sessionId, session);
		return sessionId;
	}

	@Override
	protected Session doReadSession(Serializable sessionId) {
		System.out.println("Reading session...");
		return sessionBuffer.get(sessionId);
	}

}
