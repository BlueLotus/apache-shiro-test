package com.shiro.demo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Service;

@Service(value = "annoService")
public class HelloAnno {
	
	@Autowired
	private SecurityManager securityManager;
	
	@RequiresUser
	@RequiresAuthentication
	@RequiresPermissions({"p1"})
	@RequiresRoles("admin")
	public void doSomething() {
		System.out.println("Do something...");
	}

	public void login() {
		UsernamePasswordToken token = new UsernamePasswordToken("carl", "qwert1234");
		token.setRememberMe(true);
		SecurityUtils.setSecurityManager(securityManager);
		Subject currentUser = SecurityUtils.getSubject();
		currentUser.login(token);
		System.out.println("Login successfully...");
	}

	public static void main(String[] args) {
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext("classpath:app-context.xml");

		HelloAnno helloAnno = (HelloAnno) ctx.getBean("annoService");
		helloAnno.login();
		helloAnno.doSomething();
		ctx.close();
	}

}
