package com.shiro.demo.listener;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class CustomSessionListener implements SessionListener {

	@Override
	public void onStart(Session session) {
		System.out.println("Session on start...");
	}

	@Override
	public void onStop(Session session) {
		System.out.println("Session on stop...");
	}

	@Override
	public void onExpiration(Session session) {
		System.out.println("Session on expiration...");
	}

}
