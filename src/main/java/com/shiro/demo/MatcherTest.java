package com.shiro.demo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class MatcherTest {

	public static void main(String[] args) {
		String admin = "carl";
		String passwordForAdmin = "qwert1234";
		
		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiroInitForMatcherTest.ini");
		
		SecurityManager securityManager = factory.getInstance();
		
		SecurityUtils.setSecurityManager(securityManager);
		
		UsernamePasswordToken token = new UsernamePasswordToken(admin, passwordForAdmin);
		token.setRememberMe(true);
		
		Subject currentUser = SecurityUtils.getSubject();
		currentUser.login(token);
		System.out.println("Current principal: " + currentUser.getPrincipal());
		System.out.println("is remeber: " + currentUser.isRemembered());
		System.out.println("is authenticated: " + currentUser.isAuthenticated());
		
		boolean permissionFlag = currentUser.isPermittedAll("p1", "p2");
		System.out.println("Permission: " + permissionFlag);
		
		System.out.println("Has operator: " + currentUser.hasRole("admin"));
		currentUser.logout();
	}

}
