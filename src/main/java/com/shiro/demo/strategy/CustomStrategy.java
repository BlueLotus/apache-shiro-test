package com.shiro.demo.strategy;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.pam.AbstractAuthenticationStrategy;
import org.apache.shiro.realm.Realm;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class CustomStrategy extends AbstractAuthenticationStrategy {
	
	public AuthenticationInfo afterAttempt(Realm realm, AuthenticationToken token, 
			AuthenticationInfo singleRealmInfo, AuthenticationInfo aggregateInfo, Throwable t) throws AuthenticationException {
		
		if (realm.getName().equals("customRealm2")) {
			if (singleRealmInfo == null || singleRealmInfo.getPrincipals() == null) {
				throw new AuthenticationException("Realm2 auth failed.");
			}
		}
		
		return super.afterAttempt(realm, token, singleRealmInfo, aggregateInfo, t);
		
	}
}
