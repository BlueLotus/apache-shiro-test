package com.shiro.demo.realm;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class FirstCustomRealm extends AuthorizingRealm {
	
	private String originalHexPassword = "0542d8871f7076b1c6ac0aa89a8ac6494f965f4edc7ade6c11c05ca516264b1c";
	private String saltedPassword = "41db45a719ed859e20a4051874f80980c1f52e32f30f97282d93552b3c3847a3";
	private String salt = "carl";
	
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		
		String userName = (String) getAvailablePrincipal(principals);
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		
		Set<String> permissions = new HashSet<String>();
		permissions.add("p1");
		permissions.add("p2");
		info.setStringPermissions(permissions);
		
		Set<String> roles = new HashSet<String>();
		roles.add("admin");
		roles.add("operator");
		info.setRoles(roles);
		
		return info;
		
	}
	
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		
		UsernamePasswordToken upToken = (UsernamePasswordToken) token;
		String username = upToken.getUsername();
		String password = String.valueOf(upToken.getPassword());
		
		if(username.equals("miffy")) {
			System.out.println("You!!!!");
			throw new AuthenticationException("Realm1 Failed!");
		}

		SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(username, saltedPassword.toCharArray(), ByteSource.Util.bytes(salt.getBytes()), getName());
		return info;
		
	}
}