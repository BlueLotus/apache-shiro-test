package com.shiro.demo.cipher.gen;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.crypto.hash.Sha512Hash;

public class Encryptor {
	
	public String obtainPasswordAfterEncryption(String originalPassword, String encryptionAlg) {
		
		String result = null;
		
		switch (encryptionAlg) {
		case "SHA256":
			result = new Sha256Hash(originalPassword).toHex();
			break;
		case "SHA512":
			result = new Sha512Hash(originalPassword).toHex();
			break;
		default:
			result = originalPassword;
			break;
		}
		
		return result;
	}
	
}
